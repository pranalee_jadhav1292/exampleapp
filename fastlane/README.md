fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios hello
```
fastlane ios hello
```

### ios pre_commit_lint
```
fastlane ios pre_commit_lint
```

### ios push_branch
```
fastlane ios push_branch
```

### ios merge_branch
```
fastlane ios merge_branch
```

### ios import_localizations
```
fastlane ios import_localizations
```

### ios jenkins_merge_request
```
fastlane ios jenkins_merge_request
```
Validate the framework in merge request
### ios create_documentation
```
fastlane ios create_documentation
```
Create API documentation
### ios create_test_report
```
fastlane ios create_test_report
```
Run validation tests

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
