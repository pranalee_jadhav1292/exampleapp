#! /usr/local/bin/bash

XLOC_FOLDER_PATH="./WeblateResources_XLOC_FILES"
XLIFF_FOLDER_PATH="./WeblateResources_ONLY_XLIFF_FILES"
WEBLATE_FOLDER_PATH="../WeblateResources"
XCODE_PROJ_FOLDER_PATH="../JSMAIOSLocalization/JSMAIOSLocalization.xcodeproj"

languages=("ar" "cs" "da" "de" "el" "en" "es" "fi" "fr" "id" "it" "he" "ja" "ko" "ms" "nb" "nl" "pl" "pt" "ro" "ru" "sv" "th" "tr" "uk" "vi" "zh-Hans" "zh-Hant" "zh-Hant-HK" "zh-Hant-TW" "en-AU" "en-CA" "en-GB" "en-IE" "en-NZ" "es-AR" "es-CO" "es-ES" "es-US" "es-UY" "hu" "fr-CA" "pt-PT")

declare -A languageMap

languageMap=(["in"]="id" ["iw"]="he" ["no"]="nb" ["zh"]="zh-Hans" ["zh_TW"]="zh-Hant zh-Hant-TW" ["zh_HK"]="zh-Hant-HK" ["en_AU"]="en-AU" ["en_CA"]="en-CA" ["en_GB"]="en-GB" ["en_IE"]="en-IE" ["en_NZ"]="en-NZ" ["es_AR"]="es-AR" ["es_CO"]="es-CO" ["es_ES"]="es-ES" ["es_US"]="es-US" ["es_UY"]="es-UY" ["fr_CA"]="fr-CA" ["pt_PT"]="pt-PT")
