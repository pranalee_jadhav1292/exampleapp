"Back" = "Back";
"OK" = "OK";
"Confirm" = "Confirm";
"Cancel" = "Cancel";
"Close" = "Close";

"location_services_off" = "Location Services Off";
"location_services_message" = "Enable location services by going to Settings > Privacy > Location Services";
"location_services_restricted_message" = "Location service is restricted on this device.";
"invalid_country_selection" = "The country you selected is not supported at this time.";
"Home" = "Home";
"continue_as_guest" = "Continue as guest";
"skip_login" = "Skip";
"unable_to_connect_message" = "Unable to connect to Indeed.";
"Retry" = "Retry";
"contact_us" = "Contact us";
"turn_on_notification" = "Turn on notifications";
"push_notification_message" = "Push notifications are turned off. Please enable push notifications to receive these alerts.";
"Settings" = "Settings";
"try_again" = "Try Again";// loading error default button
"terms_of_service_message" = "By using Indeed you agree to our new <a {0}>Privacy Policy</a>, <a {1}>Cookie Policy</a>, and <a {2}>Terms</a>, which we encourage you to review.";
"ccpa_message" = "Do Not Sell My Personal Information";
"edit" = "Edit";
"dismiss_message" = "Dismiss";
"employers_block_message" = "To access this, please visit the main Indeed site.";
"optional" = "optional";

//-----------------------------------
//MARK: - app review

"review_title" = "Tell us what you think";
"review_message" = "If you've enjoyed using Indeed on your mobile device we'd love to hear from you.";
"rate" = "Rate \"Indeed Jobs\"";
"remind_me_later" = "Remind me later";
"no_thanks" = "No thanks";

//-----------------------------------
//MARK: - Messages
"conversation_list_sign_in" = "Sign in to your Indeed account to view messages";
"conversation_list_title" = "Inbox";
"conversation_archived_title" = "Archived";
"conversation_list_error_title" = "Well, that was unexpected";
"conversation_list_error_subtitle" = "We weren’t able to load that information.";
"conversation_list_retry" = "Refresh now";
"conversation_list_find_jobs" = "Find jobs";
"conversation_list_upload_resume" = "Upload your resume";
"conversation_list_welcome_title" = "Welcome to Messages";
"conversation_list_welcome_subtitle" = "When an employer contacts you, you will see messages here.";
"conversation_list_empty_archive" = "You have no archived messages";
"conversation_list_edit" = "Edit";
"conversation_list_done" = "Done";
"messaging_generic_employer" = "Employer";
"messaging_unknown_name" = "Unknown";
"conversation_view_send_placeholder" = "Write your response...";
"conversation_unknown_participant" = "Unknown";
"conversation_you" = "You";
"conversation_view_job" = "View Job";
"conversation_send_failed" = "Your message wasn't delivered.";
"conversation_list_reminder" = "Reminder";
"conversation_archive" = "Archive";
"conversation_unarchive_long" = "Unarchive Conversation";
"conversation_archive_long" = "Archive Conversation";
"conversation_applied_time" = "You applied to this position on %@.";
"conversation_resume_contact" = "%1$@ connected with you based on your <a %2$@>resume on Indeed</a>.";
"conversation_update_error" = "An error occurred while trying to update conversation.";
"conversation_more_actions" = "More actions";

//-----------------------------------
//MARK: - Report Conversation
// Report conversation
"conv_report_title" = "Report";
"conv_report_question" = "Why are you reporting this message?";
"conv_report_type_advertisement" = "Advertisement";
"conv_report_type_advertisement_description" = "Tried to sell me something";
"conv_report_type_offensive" = "Discriminatory or offensive";
"conv_report_type_offensive_description" = "Used biased or inappropriate language";
"conv_report_type_fake" = "Fraud";
"conv_report_type_fake_description" = "Might be fake or someone posting as this company";
"conv_report_type_inaccurate" = "Misleading";
"conv_report_type_inaccurate_description" = "Didn't match the job description requirements";
"conv_report_type_uninterested" = "Not a fit or did not apply";
"conv_report_type_uninterested_description" = "Won't pursue this opportunity";
"conv_report_type_other" = "Other";
"conv_report_type_other_description" = "It's something else";
"conv_report_details_title" = "Report details";
"conv_report_details_ask" = "Would you like to provide details?";
"conv_report_enter_placeholder" = "Enter a message";
"conv_report_max_chars" = "Maximum %1$@ characters, %2$@ characters remaining.";
"conv_report_submit" = "Submit feedback";

//-----------------------------------
//MARK: - Native Home Page

"popular_searches" = "Popular Searches";
"recent_searches" = "Recent searches";
"sign_in" = "Sign in";
"my_jobs" = "My Jobs";
"create_your_resume" = "Create Your Resume";
"find_jobs" = "Find Jobs";
"company_reviews" = "Company Reviews";
"find_salaries" = "Find Salaries";
"change_country" = "Change Country";
"help_center" = "Help Center";
"account_settings" = "Account Settings";
"inbox" = "Inbox";
"my_subscriptions" = "My Subscriptions";
"my_resume" = "My Resume";
"select_country" = "Select Country";
"placeholder_searchField" = "Find Jobs near Current Location";
"messages" = "Messages";
"help_center" = "Help Center";
"notifications" = "Notifications";
"select_all" = "Select all";
"delete_selected" = "Delete Selected";
"upload_resume" = "Upload Resume";

"job_alert_email_update" = "Get Email Updates";

//-----------------------------------
//MARK: - Reengagement local push notification

"reengagement_push_title" = "Ready to work?";
"reengagement_push_subtitle" = "Employers are still hiring for many positions";

//-----------------------------------
//MARK: - Native push primer

"pushprimer_title" = "Get Job Updates";
"pushprimer_subtitle" = "Indeed Jobs sends push notifications to:";
"pushprimer_bullets" = "Keep you up to date with the latest jobs.\nUpdate you about jobs you’ve applied to.\nLet you know when you’ve got a new message from an employer.";
"pushprimer_button_register" = "Enable push notifications";
"pushprimer_button_notnow" = "Skip for now";

//-----------------------------------
//MARK: - Error alerts

// forced upgrade error
"forced_upgrade_title" = "Update Required";
"force_upgrade_message" = "Indeed Jobs is out of date. Please visit the App Store to update to the latest version.";
"force_upgrade_button" = "Update";

// Loading/startup api failure
"connection_error_title" = "Connection Error"; // loading error title
"connection_error_message" = "Sorry, we cannot reach the internet. Please check your connection."; // loading error message


// API error - server failure
"server_connection_error_title" = "Connection Error";
"server_connection_error_message" = "Sorry, we cannot reach Indeed as the server is temporarily unavailable.";


// sign in failure alert
"signin_failed" = "Sign-in failed";
"signin_error_message" = "Something went wrong and we were unable to sign you in.";

//search filter
"Reset" = "Reset";

// rate limiting
"rate_limiting_error_title" = "Unusual Activity";
"rate_limiting_error_message" = "We've detected some unusual activity. Please solve the reCAPTCHA below.";
 
//Apply
"Apply" = "Apply";
"discard_apply" = "Discard your application?";
"restart_apply" = "You'll have to restart if you apply again.";

//Search Overlay
"find_jobs" = "Find Jobs";

// Regpromo
"have_an_account" = "Have an account?";
"no1_jobsearch_site" = "The world's #1 job search site";
"create_an_account" = "Create an account";
"join_now" = "Join now";
"continue_without_an_account" = "Continue without an account";
"regpromo_legal_text" = "By using Indeed, you agree and consent to our: <a {0}>Terms of Service</a> - <a {1}>Cookie Policy</a> - <a {2}>Privacy Policy</a>";
"regpromo_consent_text" = "By using Indeed, you agree and consent to our:";
"citation_link_text" = "*All references can be found here";
"terms_of_service" = "Terms of Service";
"cookie_policy" = "Cookie Policy";
"privacy_policy" = "Privacy Policy";
"terms" = "Terms";

//Onboarding
"onboarding_page1" = "Search millions of jobs in one place";
"onboarding_page2" = "Access company reviews and salary information";
"onboarding_page3" = "Apply to jobs from your phone with your saved resume";
"onboarding_page4" = "Take your application further by messaging employers";
"onboarding_getstarted" = "Get started";
"onboarding_skip" = "Skip";

//App Review
"app_review_enjoying_indeed" = "Enjoying the Indeed app?";
"app_review_good_to_know" = "That's good to know";
"app_review_glad_to_help" = "Glad we're helping";
"app_review_help_us_improve" = "Have thoughts to share?";
"app_review_hear_your_feedback" = "Your feedback is valuable to us";
"app_review_yes" = "Yes";
"app_review_not_really" = "Not really";
"app_review_contact_us" = "Contact us";

//3d touch Quick Actions
"quick_actions_search" = "Search";
"quick_actions_saved_jobs" = "Saved Jobs";

//MARK: - Accessibility lables
"a11y_open_safari" = "Open in Safari";
"a11y_web_address" = "Address";
"a11y_https_enabled" = "secure and validated";
"a11y_loading_announcement" = "page is loading";
"a11y_apply_keyboard_suggestion" = "%@ Indeed suggestion";
"a11y_apply_keyboard_icon" = "Suggestions from your Indeed résumé";

"Share" = "Share";
"Reload" = "Reload";
"Forward" = "Forward";

"try_again" = "Try Again";

"select_country" = "Select Country";

//MARK: - Global Nav
"gnav_signin_prompt" = "Have an account?";
"gnav_signin_link" = "Sign In";
"gnav_menu_createaccount" = "Create account";
"gnav_menu_findjobs" = "Find Jobs";
"gnav_menu_companyreviews" = "Company Reviews";
"gnav_menu_findsalaries" = "Find Salaries";
"gnav_menu_resume" = "Resume";
"gnav_menu_myjobs" = "My Jobs";
"gnav_menu_mysubscriptions" = "My Subscriptions";
"gnav_menu_mycontributions" = "My Contributions";
"gnav_menu_account" = "Account";
"gnav_menu_changecountry" = "Change Country";
"gnav_menu_helpcenter" = "Help Center";
"gnav_menu_debug" = "DEBUG View";

//MARK: - Theme Accessibility support
"a11y_open_safari" = "Open in Safari";
"a11y_navbar_more" = "More actions";
"a11y_navbar_menu" = "Menu";
"a11y_navbar_notifications" = "Notifications";
"a11y_navbar_savedjobs" = "Saved Jobs";
"a11y_navbar_messages" = "Messages";

"upload_confirmation_title" = "Upload this file?";
"button_upload" = "Upload";

"upload_signin_title" = "Sign in or create an account to upload your resume";
"upload_signin_subtitle" = "Having a resume with Indeed will let you quickly apply for jobs";
"button_signin" = "Sign In";
"button_create_account" = "Create an Account";


"upload_file_exist_title" = "Replace your existing resume?";
"upload_file_exist_subtitle" = "Indeed only stores one resume at a time. If you upload or create a new one, your existing resume and any changes you've made will be deleted";
"button_replace_resume" = "Replace my existing resume";
"button_cancel" = "Cancel";

"upload_processing_title" = "Resume Processing";
"upload_processing_subtitle" = "Your resume is uploading";
"hint_job_seekers" = "30% of the global workforce is made up of active job seekers.";

"upload_success_title" = "Upload Successful";
"upload_success_subtitle" = "Make your resume public so employers can reach out to you.";
"button_make_public" = "Make my resume public";
"button_later" = "Maybe later";
"explanation_title_resume_reachable" = "When your resume is public:";
"explanation_text_resume_reachable" = "It's available for anyone to find\nEmployers and other registered users can contact you through Indeed\nYour phone number and address stay hidden\nYou can make your resume private again at any time\nView our privacy policy";

"upload_error_title" = "An error occurred uploading your resume";
"upload_error_subtitle" = "Please try again in a few moments";
"button_try_again" = "Try Again";

"resume_upload_not_supported" = "We're sorry, but resume upload is not supported for your region and/or language.";

"make_resume_public_error_title" = "An error occurred contacting the server. Please check your internet connection and try again.";

// sign in failure alert
"signin_failed" = "Sign-in failed";
"signin_error_message" = "Something went wrong and we were unable to sign you i";
