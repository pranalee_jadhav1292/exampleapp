#! /usr/local/bin/bash

source helper.sh

#step 1: delete directories
rm -rf $XLOC_FOLDER_PATH/*
rm -rf $WEBLATE_FOLDER_PATH/*

#step 2: export localization to XLOC_FOLDER_PATH
for lname in  ${languages[@]};
do
    xcodebuild -exportLocalizations -project $XCODE_PROJ_FOLDER_PATH -localizationPath $XLOC_FOLDER_PATH -exportLanguage $lname
done 

#step 3: since weblate only accepts xliff files, copy all the xliff files from XLOC_FOLDER_PATH to XLIFF_FOLDER_PATH. 
find "$XLOC_FOLDER_PATH" -name \*.xliff -print0 | xargs -I{} -0 cp -v {} "$WEBLATE_FOLDER_PATH/"

#step 4: rename files to the names supported by the weblate api
for fname in ${!languageMap[@]};
do
  weblate_fname="$fname.xliff"
  for xcode_fname in ${languageMap[$fname]}; do
    mv "$WEBLATE_FOLDER_PATH/$xcode_fname.xliff" $WEBLATE_FOLDER_PATH/$weblate_fname
  done
done