#! /usr/local/bin/bash
source helper.sh

#step 1: delete directories
rm -rf $XLOC_FOLDER_PATH/*
rm -rf $XLIFF_FOLDER_PATH/*

#step 2: create xloc files at XLOC_FOLDER_PATH
for lname in  ${languages[@]};
do
    xcodebuild -exportLocalizations -project $XCODE_PROJ_FOLDER_PATH -localizationPath $XLOC_FOLDER_PATH -exportLanguage $lname
done

cp -a $WEBLATE_FOLDER_PATH/. $XLIFF_FOLDER_PATH/

#step 1: rename files to the names supported by the xcode
for fname in ${!languageMap[@]};
do
  weblate_fname="$fname.xliff"
  for xcode_fname in ${languageMap[$fname]}; do
    cp $XLIFF_FOLDER_PATH/$weblate_fname "$XLIFF_FOLDER_PATH/$xcode_fname.xliff"
  done
  rm "$XLIFF_FOLDER_PATH/$weblate_fname"
done

#step 2: copy all the xliff files from XLIFF_FOLDER_PATH to respective languages folder in XLOC_FOLDER_PATH.
for f in $XLIFF_FOLDER_PATH/*.xliff; 
do
    filename=$(basename $f .xliff)
    dir="$XLOC_FOLDER_PATH/$filename.xcloc/Localized Contents/$filename.xliff"
    mv "$f" "$dir"
done

#step 3: import localization
for lname in  ${languages[@]};
do
    xcodebuild -importLocalizations -localizationPath "$XLOC_FOLDER_PATH/$lname.xcloc" -project $XCODE_PROJ_FOLDER_PATH
done 