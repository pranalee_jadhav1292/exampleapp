//
//  JSMAIOSLocalizationTests.swift
//  JSMAIOSLocalizationTests
//
//  Created by Pranalee Jadhav on 12/19/20.
//

import XCTest
@testable import JSMAIOSLocalization

class JSMAIOSLocalizationTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testStrings() {
        let aa = L10n.explanationTextResumeReachable
        XCTAssertNotEqual(aa, "It's available for anyone to find\nEmployers and other registered users can contact you through Indeed\nYour phone number and address stay hidden\nYou can make your resume private again at any time\nView our privacy policy")
    }
    /*func testLocalizations() {
        let locales = ["ar","cs","da","de","el","en","es","fi","fr","id","it","he","ja","ko","ms","nb","nl","pl","pt","ro","ru","sv","th","tr","uk","vi","zh-Hans","zh-Hant","zh-Hant-HK","zh-Hant-TW","en-AU","en-CA","en-GB","en-IE","en-NZ","es-AR","es-CO","es-ES","es-US","es-UY","hu","fr-CA","pt-PT"]
        for locale in locales {
            verifyLocalization(localeIdentifier: locale)
        }
    }
    
    func verifyLocalization(localeIdentifier: String) {
        
        /*guard let path = Bundle.module.path(forResource: localeIdentifier, ofType: "lproj"), let bundle = Bundle(path: path) else {
                XCTFail("Localization is missing for \(localeIdentifier)"); return
            }

        let string = bundle.localizedString(forKey: "Back", value: nil, table: nil)

        XCTAssertFalse(string.isEmpty)
        XCTAssertNotEqual(string, "back")*/
    }*/
}
