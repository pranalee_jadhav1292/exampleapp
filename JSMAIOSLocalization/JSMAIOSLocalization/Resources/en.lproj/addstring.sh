#! /usr/local/bin/bash

filename=Localizable.strings


if ! grep -R "^[#]*\s*\"${thekey}\" = .*" $filename > /dev/null; then
  echo "APPENDING because '${thekey}' not found"
  echo "\"$thekey\" = \"$newvalue\"" >> $filename
else
  echo "SETTING because '${thekey}' found already"
  sed -ir "s/^[#]*\s*\"${thekey}\" = .*/\"$thekey\" = \"$newvalue\"/" $filename
fi