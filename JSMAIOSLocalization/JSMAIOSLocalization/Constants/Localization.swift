//
//  Strings.swift
//  JSMALocalization
//
//  Created by Pranalee Jadhav on 9/20/20.
//  Copyright © 2020 Pranalee Jadhav. All rights reserved.
//

import Foundation


public enum Localization: String {
    
    case done
    case back
    case OK
    case confirm
    case close
    case location_services_off
    case location_services_message
    case location_services_restricted_message
    case invalid_country_selection
    case home
    case continue_as_guest
    case skip_login
    case unable_to_connect_message
    case retry
    case contact_us
    case push_notification_message
    case settings
    case try_again
    case terms_of_service_message
    case ccpa_message
    case edit
    case dismiss_message
    case employers_block_message
    case optional
    
    //-----------------------------------
    //MARK: - app review
    
    case review_title
    case review_message
    case remind_me_later
    case rate
    case no_thanks
    
    //-----------------------------------
    //MARK: - Messages
    
    case conversation_list_sign_in
    case conversation_list_title
    case conversation_archived_title
    case conversation_list_error_title
    case conversation_list_error_subtitle
    case conversation_list_retry
    case conversation_list_find_jobs
    case conversation_list_upload_resume
    case conversation_list_welcome_title
    case conversation_list_welcome_subtitle
    case conversation_list_empty_archive
    case conversation_list_edit
    case conversation_list_done
    case messaging_generic_employer
    case messaging_unknown_name
    case conversation_view_send_placeholder
    case conversation_unknown_participant
    case conversation_you
    case conversation_view_job
    case conversation_send_failed
    case conversation_list_reminder
    case conversation_archive
    case conversation_unarchive_long
    case conversation_archive_long
    case conversation_applied_time
    case conversation_resume_contact
    case conversation_update_error
    case conversation_more_actions

    //-----------------------------------
    //MARK: - Report Conversation
    
    case conv_report_title
    case conv_report_question
    case conv_report_type_advertisement
    case conv_report_type_advertisement_description
    case conv_report_type_offensive
    case conv_report_type_offensive_description
    case conv_report_type_fake
    case conv_report_type_fake_description
    case conv_report_type_inaccurate
    case conv_report_type_inaccurate_description
    case conv_report_type_uninterested
    case conv_report_type_uninterested_description
    case conv_report_type_other
    case conv_report_type_other_description
    case conv_report_details_title
    case conv_report_details_ask
    case conv_report_enter_placeholder
    case conv_report_max_chars
    case conv_report_submit
    
    //-----------------------------------
    //MARK: - Native Home Page
    
    case popular_searches
    case recent_searches
    case sign_in
    case my_jobs
    case create_your_resume
    case find_jobs
    case company_reviews
    case find_salaries
    case change_country
    case help_center
    case account_settings
    case inbox
    case my_subscriptions
    case my_resume
    case select_country
    case placeholder_searchField
    case messages
    case notifications
    case select_all
    case delete_selected
    case upload_resume
    
    case job_alert_email_update

    //-----------------------------------
    //MARK: - Reengagement local push notification

    case reengagement_push_title
    case reengagement_push_subtitle
    
    //-----------------------------------
    //MARK: - Native push primer

    case pushprimer_title
    case pushprimer_subtitle
    case pushprimer_bullets
    case pushprimer_button_register
    case pushprimer_button_notnow
    
    //-----------------------------------
    //MARK: - Error alerts

    // forced upgrade error
    case forced_upgrade_title
    case force_upgrade_message
    case force_upgrade_button

    // Loading/startup api failure
    case connection_error_title
    case connection_error_message

    // API error - server failure
    case server_connection_error_title
    case server_connection_error_message

    // sign in failure alert
    case signin_failed
    case signin_error_message

    //search filter
    case reset

    // rate limiting
    case rate_limiting_error_title
    case rate_limiting_error_message

    //Apply
    case apply
    case discard_apply
    case restart_apply

    //-----------------------------------
    //MARK: - Reg Promo
    
    case have_an_account
    case no1_jobsearch_site
    case create_an_account
    case join_now
    case continue_without_an_account
    case regpromo_legal_text
    case terms_of_service
    case cookie_policy
    case privacy_policy
    case terms
    case regpromo_consent_text
    case citation_link_text

    //-----------------------------------
    //MARK: - App Review
    
    case app_review_enjoying_indeed
    case app_review_good_to_know
    case app_review_glad_to_help
    case app_review_help_us_improve
    case app_review_hear_your_feedback
    case app_review_yes
    case app_review_not_really
    case app_review_contact_us

    //-----------------------------------
    //MARK: - 3d Quick Actions
    
    case quick_actions_search
    case quick_actions_saved_jobs

    case share
    case reload
    case forward

    //-----------------------------------
    //MARK: - Global Nav
    
    case gnav_signin_prompt
    case gnav_signin_link
    case gnav_menu_createaccount
    case gnav_menu_findjobs
    case gnav_menu_companyreviews
    case gnav_menu_findsalaries
    case gnav_menu_resume
    case gnav_menu_myjobs
    case gnav_menu_mysubscriptions
    case gnav_menu_mycontributions
    case gnav_menu_account
    case gnav_menu_changecountry
    case gnav_menu_helpcenter
    case gnav_menu_debug
    
    case upload_confirmation_title
    case button_upload

    case upload_signin_title
    case upload_signin_subtitle
    case button_signin
    case button_create_account

    case upload_file_exist_title
    case upload_file_exist_subtitle
    case button_replace_resume
    case button_cancel

    case upload_processing_title
    case upload_processing_subtitle
    case hint_job_seekers

    case upload_success_title
    case upload_success_subtitle
    case button_make_public
    case button_later
    case explanation_title_resume_reachable
    case explanation_text_resume_reachable
    case upload_error_title
    case upload_error_subtitle
    case button_try_again

    case resume_upload_not_supported
    case make_resume_public_error_title
    
    //MARK: - Accessibility lables
    
    case a11y_open_safari
    case a11y_web_address
    case a11y_https_enabled
    case a11y_loading_announcement
    case a11y_apply_keyboard_suggestion
    case a11y_apply_keyboard_icon
    case a11y_navbar_more
    case a11y_navbar_menu
    case a11y_navbar_notifications
    case a11y_navbar_savedjobs
    case a11y_navbar_messages
    
    //MARK: - Onboarding
    
    case onboarding_page1
    case onboarding_page2
    case onboarding_page3
    case onboarding_page4
    case onboarding_getstarted
    
    var string: String {
        let a = #"a raw string containing \r\n"#
    
          return rawValue.localized()
    }
    
    public func localized() -> String {
        return self.rawValue.localized(withComment: "")
    }
}
