// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// Suggestions from your Indeed résumé
  internal static let a11yApplyKeyboardIcon = L10n.tr("Localizable", "a11y_apply_keyboard_icon")
  /// %@ Indeed suggestion
  internal static func a11yApplyKeyboardSuggestion(_ p1: Any) -> String {
    return L10n.tr("Localizable", "a11y_apply_keyboard_suggestion", String(describing: p1))
  }
  /// secure and validated
  internal static let a11yHttpsEnabled = L10n.tr("Localizable", "a11y_https_enabled")
  /// page is loading
  internal static let a11yLoadingAnnouncement = L10n.tr("Localizable", "a11y_loading_announcement")
  /// Menu
  internal static let a11yNavbarMenu = L10n.tr("Localizable", "a11y_navbar_menu")
  /// Messages
  internal static let a11yNavbarMessages = L10n.tr("Localizable", "a11y_navbar_messages")
  /// More actions
  internal static let a11yNavbarMore = L10n.tr("Localizable", "a11y_navbar_more")
  /// Notifications
  internal static let a11yNavbarNotifications = L10n.tr("Localizable", "a11y_navbar_notifications")
  /// Saved Jobs
  internal static let a11yNavbarSavedjobs = L10n.tr("Localizable", "a11y_navbar_savedjobs")
  /// Open in Safari
  internal static let a11yOpenSafari = L10n.tr("Localizable", "a11y_open_safari")
  /// Address
  internal static let a11yWebAddress = L10n.tr("Localizable", "a11y_web_address")
  /// Account Settings
  internal static let accountSettings = L10n.tr("Localizable", "account_settings")
  /// Contact us
  internal static let appReviewContactUs = L10n.tr("Localizable", "app_review_contact_us")
  /// Enjoying the Indeed app?
  internal static let appReviewEnjoyingIndeed = L10n.tr("Localizable", "app_review_enjoying_indeed")
  /// Glad we're helping
  internal static let appReviewGladToHelp = L10n.tr("Localizable", "app_review_glad_to_help")
  /// That's good to know
  internal static let appReviewGoodToKnow = L10n.tr("Localizable", "app_review_good_to_know")
  /// Your feedback is valuable to us
  internal static let appReviewHearYourFeedback = L10n.tr("Localizable", "app_review_hear_your_feedback")
  /// Have thoughts to share?
  internal static let appReviewHelpUsImprove = L10n.tr("Localizable", "app_review_help_us_improve")
  /// Not really
  internal static let appReviewNotReally = L10n.tr("Localizable", "app_review_not_really")
  /// Yes
  internal static let appReviewYes = L10n.tr("Localizable", "app_review_yes")
  /// Apply
  internal static let apply = L10n.tr("Localizable", "Apply")
  /// Back
  internal static let back = L10n.tr("Localizable", "Back")
  /// Cancel
  internal static let buttonCancel = L10n.tr("Localizable", "button_cancel")
  /// Create an Account
  internal static let buttonCreateAccount = L10n.tr("Localizable", "button_create_account")
  /// Maybe later
  internal static let buttonLater = L10n.tr("Localizable", "button_later")
  /// Make my resume public
  internal static let buttonMakePublic = L10n.tr("Localizable", "button_make_public")
  /// Replace my existing resume
  internal static let buttonReplaceResume = L10n.tr("Localizable", "button_replace_resume")
  /// Sign In
  internal static let buttonSignin = L10n.tr("Localizable", "button_signin")
  /// Try Again
  internal static let buttonTryAgain = L10n.tr("Localizable", "button_try_again")
  /// Upload
  internal static let buttonUpload = L10n.tr("Localizable", "button_upload")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "Cancel")
  /// Do Not Sell My Personal Information
  internal static let ccpaMessage = L10n.tr("Localizable", "ccpa_message")
  /// Change Country
  internal static let changeCountry = L10n.tr("Localizable", "change_country")
  /// *All references can be found here
  internal static let citationLinkText = L10n.tr("Localizable", "citation_link_text")
  /// Close
  internal static let close = L10n.tr("Localizable", "Close")
  /// Company Reviews
  internal static let companyReviews = L10n.tr("Localizable", "company_reviews")
  /// Confirm
  internal static let confirm = L10n.tr("Localizable", "Confirm")
  /// Sorry, we cannot reach the internet. Please check your connection.
  internal static let connectionErrorMessage = L10n.tr("Localizable", "connection_error_message")
  /// Connection Error
  internal static let connectionErrorTitle = L10n.tr("Localizable", "connection_error_title")
  /// Contact us
  internal static let contactUs = L10n.tr("Localizable", "contact_us")
  /// Continue as guest
  internal static let continueAsGuest = L10n.tr("Localizable", "continue_as_guest")
  /// Continue without an account
  internal static let continueWithoutAnAccount = L10n.tr("Localizable", "continue_without_an_account")
  /// Would you like to provide details?
  internal static let convReportDetailsAsk = L10n.tr("Localizable", "conv_report_details_ask")
  /// Report details
  internal static let convReportDetailsTitle = L10n.tr("Localizable", "conv_report_details_title")
  /// Enter a message
  internal static let convReportEnterPlaceholder = L10n.tr("Localizable", "conv_report_enter_placeholder")
  /// Maximum %1$@ characters, %2$@ characters remaining.
  internal static func convReportMaxChars(_ p1: Any, _ p2: Any) -> String {
    return L10n.tr("Localizable", "conv_report_max_chars", String(describing: p1), String(describing: p2))
  }
  /// Why are you reporting this message?
  internal static let convReportQuestion = L10n.tr("Localizable", "conv_report_question")
  /// Submit feedback
  internal static let convReportSubmit = L10n.tr("Localizable", "conv_report_submit")
  /// Report
  internal static let convReportTitle = L10n.tr("Localizable", "conv_report_title")
  /// Advertisement
  internal static let convReportTypeAdvertisement = L10n.tr("Localizable", "conv_report_type_advertisement")
  /// Tried to sell me something
  internal static let convReportTypeAdvertisementDescription = L10n.tr("Localizable", "conv_report_type_advertisement_description")
  /// Fraud
  internal static let convReportTypeFake = L10n.tr("Localizable", "conv_report_type_fake")
  /// Might be fake or someone posting as this company
  internal static let convReportTypeFakeDescription = L10n.tr("Localizable", "conv_report_type_fake_description")
  /// Misleading
  internal static let convReportTypeInaccurate = L10n.tr("Localizable", "conv_report_type_inaccurate")
  /// Didn't match the job description requirements
  internal static let convReportTypeInaccurateDescription = L10n.tr("Localizable", "conv_report_type_inaccurate_description")
  /// Discriminatory or offensive
  internal static let convReportTypeOffensive = L10n.tr("Localizable", "conv_report_type_offensive")
  /// Used biased or inappropriate language
  internal static let convReportTypeOffensiveDescription = L10n.tr("Localizable", "conv_report_type_offensive_description")
  /// Other
  internal static let convReportTypeOther = L10n.tr("Localizable", "conv_report_type_other")
  /// It's something else
  internal static let convReportTypeOtherDescription = L10n.tr("Localizable", "conv_report_type_other_description")
  /// Not a fit or did not apply
  internal static let convReportTypeUninterested = L10n.tr("Localizable", "conv_report_type_uninterested")
  /// Won't pursue this opportunity
  internal static let convReportTypeUninterestedDescription = L10n.tr("Localizable", "conv_report_type_uninterested_description")
  /// You applied to this position on %@.
  internal static func conversationAppliedTime(_ p1: Any) -> String {
    return L10n.tr("Localizable", "conversation_applied_time", String(describing: p1))
  }
  /// Archive
  internal static let conversationArchive = L10n.tr("Localizable", "conversation_archive")
  /// Archive Conversation
  internal static let conversationArchiveLong = L10n.tr("Localizable", "conversation_archive_long")
  /// Archived
  internal static let conversationArchivedTitle = L10n.tr("Localizable", "conversation_archived_title")
  /// Done
  internal static let conversationListDone = L10n.tr("Localizable", "conversation_list_done")
  /// Edit
  internal static let conversationListEdit = L10n.tr("Localizable", "conversation_list_edit")
  /// You have no archived messages
  internal static let conversationListEmptyArchive = L10n.tr("Localizable", "conversation_list_empty_archive")
  /// We weren’t able to load that information.
  internal static let conversationListErrorSubtitle = L10n.tr("Localizable", "conversation_list_error_subtitle")
  /// Well, that was unexpected
  internal static let conversationListErrorTitle = L10n.tr("Localizable", "conversation_list_error_title")
  /// Find jobs
  internal static let conversationListFindJobs = L10n.tr("Localizable", "conversation_list_find_jobs")
  /// Reminder
  internal static let conversationListReminder = L10n.tr("Localizable", "conversation_list_reminder")
  /// Refresh now
  internal static let conversationListRetry = L10n.tr("Localizable", "conversation_list_retry")
  /// Sign in to your Indeed account to view messages
  internal static let conversationListSignIn = L10n.tr("Localizable", "conversation_list_sign_in")
  /// Inbox
  internal static let conversationListTitle = L10n.tr("Localizable", "conversation_list_title")
  /// Upload your resume
  internal static let conversationListUploadResume = L10n.tr("Localizable", "conversation_list_upload_resume")
  /// When an employer contacts you, you will see messages here.
  internal static let conversationListWelcomeSubtitle = L10n.tr("Localizable", "conversation_list_welcome_subtitle")
  /// Welcome to Messages
  internal static let conversationListWelcomeTitle = L10n.tr("Localizable", "conversation_list_welcome_title")
  /// More actions
  internal static let conversationMoreActions = L10n.tr("Localizable", "conversation_more_actions")
  /// %1$@ connected with you based on your <a %2$@>resume on Indeed</a>.
  internal static func conversationResumeContact(_ p1: Any, _ p2: Any) -> String {
    return L10n.tr("Localizable", "conversation_resume_contact", String(describing: p1), String(describing: p2))
  }
  /// Your message wasn't delivered.
  internal static let conversationSendFailed = L10n.tr("Localizable", "conversation_send_failed")
  /// Unarchive Conversation
  internal static let conversationUnarchiveLong = L10n.tr("Localizable", "conversation_unarchive_long")
  /// Unknown
  internal static let conversationUnknownParticipant = L10n.tr("Localizable", "conversation_unknown_participant")
  /// An error occurred while trying to update conversation.
  internal static let conversationUpdateError = L10n.tr("Localizable", "conversation_update_error")
  /// View Job
  internal static let conversationViewJob = L10n.tr("Localizable", "conversation_view_job")
  /// Write your response...
  internal static let conversationViewSendPlaceholder = L10n.tr("Localizable", "conversation_view_send_placeholder")
  /// You
  internal static let conversationYou = L10n.tr("Localizable", "conversation_you")
  /// Cookie Policy
  internal static let cookiePolicy = L10n.tr("Localizable", "cookie_policy")
  /// Create an account
  internal static let createAnAccount = L10n.tr("Localizable", "create_an_account")
  /// Create Your Resume
  internal static let createYourResume = L10n.tr("Localizable", "create_your_resume")
  /// Delete Selected
  internal static let deleteSelected = L10n.tr("Localizable", "delete_selected")
  /// Discard your application?
  internal static let discardApply = L10n.tr("Localizable", "discard_apply")
  /// Dismiss
  internal static let dismissMessage = L10n.tr("Localizable", "dismiss_message")
  /// Edit
  internal static let edit = L10n.tr("Localizable", "edit")
  /// To access this, please visit the main Indeed site.
  internal static let employersBlockMessage = L10n.tr("Localizable", "employers_block_message")
  /// It's available for anyone to find\nEmployers and other registered users can contact you through Indeed\nYour phone number and address stay hidden\nYou can make your resume private again at any time\nView our privacy policy
  internal static let explanationTextResumeReachable = L10n.tr("Localizable", "explanation_text_resume_reachable")
  /// When your resume is public:
  internal static let explanationTitleResumeReachable = L10n.tr("Localizable", "explanation_title_resume_reachable")
  /// Find Jobs
  internal static let findJobs = L10n.tr("Localizable", "find_jobs")
  /// Find Salaries
  internal static let findSalaries = L10n.tr("Localizable", "find_salaries")
  /// Update
  internal static let forceUpgradeButton = L10n.tr("Localizable", "force_upgrade_button")
  /// Indeed Jobs is out of date. Please visit the App Store to update to the latest version.
  internal static let forceUpgradeMessage = L10n.tr("Localizable", "force_upgrade_message")
  /// Update Required
  internal static let forcedUpgradeTitle = L10n.tr("Localizable", "forced_upgrade_title")
  /// Forward
  internal static let forward = L10n.tr("Localizable", "Forward")
  /// Account
  internal static let gnavMenuAccount = L10n.tr("Localizable", "gnav_menu_account")
  /// Change Country
  internal static let gnavMenuChangecountry = L10n.tr("Localizable", "gnav_menu_changecountry")
  /// Company Reviews
  internal static let gnavMenuCompanyreviews = L10n.tr("Localizable", "gnav_menu_companyreviews")
  /// Create account
  internal static let gnavMenuCreateaccount = L10n.tr("Localizable", "gnav_menu_createaccount")
  /// DEBUG View
  internal static let gnavMenuDebug = L10n.tr("Localizable", "gnav_menu_debug")
  /// Find Jobs
  internal static let gnavMenuFindjobs = L10n.tr("Localizable", "gnav_menu_findjobs")
  /// Find Salaries
  internal static let gnavMenuFindsalaries = L10n.tr("Localizable", "gnav_menu_findsalaries")
  /// Help Center
  internal static let gnavMenuHelpcenter = L10n.tr("Localizable", "gnav_menu_helpcenter")
  /// My Contributions
  internal static let gnavMenuMycontributions = L10n.tr("Localizable", "gnav_menu_mycontributions")
  /// My Jobs
  internal static let gnavMenuMyjobs = L10n.tr("Localizable", "gnav_menu_myjobs")
  /// My Subscriptions
  internal static let gnavMenuMysubscriptions = L10n.tr("Localizable", "gnav_menu_mysubscriptions")
  /// Resume
  internal static let gnavMenuResume = L10n.tr("Localizable", "gnav_menu_resume")
  /// Sign In
  internal static let gnavSigninLink = L10n.tr("Localizable", "gnav_signin_link")
  /// Have an account?
  internal static let gnavSigninPrompt = L10n.tr("Localizable", "gnav_signin_prompt")
  /// Have an account?
  internal static let haveAnAccount = L10n.tr("Localizable", "have_an_account")
  /// Help Center
  internal static let helpCenter = L10n.tr("Localizable", "help_center")
  /// 30% of the global workforce is made up of active job seekers.
  internal static func hintJobSeekers(_ p1: Int) -> String {
    return L10n.tr("Localizable", "hint_job_seekers", p1)
  }
  /// Home
  internal static let home = L10n.tr("Localizable", "Home")
  /// Inbox
  internal static let inbox = L10n.tr("Localizable", "inbox")
  /// The country you selected is not supported at this time.
  internal static let invalidCountrySelection = L10n.tr("Localizable", "invalid_country_selection")
  /// Get Email Updates
  internal static let jobAlertEmailUpdate = L10n.tr("Localizable", "job_alert_email_update")
  /// Join now
  internal static let joinNow = L10n.tr("Localizable", "join_now")
  /// Enable location services by going to Settings > Privacy > Location Services
  internal static let locationServicesMessage = L10n.tr("Localizable", "location_services_message")
  /// Location Services Off
  internal static let locationServicesOff = L10n.tr("Localizable", "location_services_off")
  /// Location service is restricted on this device.
  internal static let locationServicesRestrictedMessage = L10n.tr("Localizable", "location_services_restricted_message")
  /// An error occurred contacting the server. Please check your internet connection and try again.
  internal static let makeResumePublicErrorTitle = L10n.tr("Localizable", "make_resume_public_error_title")
  /// Messages
  internal static let messages = L10n.tr("Localizable", "messages")
  /// Employer
  internal static let messagingGenericEmployer = L10n.tr("Localizable", "messaging_generic_employer")
  /// Unknown
  internal static let messagingUnknownName = L10n.tr("Localizable", "messaging_unknown_name")
  /// My Jobs
  internal static let myJobs = L10n.tr("Localizable", "my_jobs")
  /// My Resume
  internal static let myResume = L10n.tr("Localizable", "my_resume")
  /// My Subscriptions
  internal static let mySubscriptions = L10n.tr("Localizable", "my_subscriptions")
  /// The world's #1 job search site
  internal static let no1JobsearchSite = L10n.tr("Localizable", "no1_jobsearch_site")
  /// No thanks
  internal static let noThanks = L10n.tr("Localizable", "no_thanks")
  /// Notifications
  internal static let notifications = L10n.tr("Localizable", "notifications")
  /// OK
  internal static let ok = L10n.tr("Localizable", "OK")
  /// Get started
  internal static let onboardingGetstarted = L10n.tr("Localizable", "onboarding_getstarted")
  /// Search millions of jobs in one place
  internal static let onboardingPage1 = L10n.tr("Localizable", "onboarding_page1")
  /// Access company reviews and salary information
  internal static let onboardingPage2 = L10n.tr("Localizable", "onboarding_page2")
  /// Apply to jobs from your phone with your saved resume
  internal static let onboardingPage3 = L10n.tr("Localizable", "onboarding_page3")
  /// Take your application further by messaging employers
  internal static let onboardingPage4 = L10n.tr("Localizable", "onboarding_page4")
  /// Skip
  internal static let onboardingSkip = L10n.tr("Localizable", "onboarding_skip")
  /// optional
  internal static let `optional` = L10n.tr("Localizable", "optional")
  /// Find Jobs near Current Location
  internal static let placeholderSearchField = L10n.tr("Localizable", "placeholder_searchField")
  /// Popular Searches
  internal static let popularSearches = L10n.tr("Localizable", "popular_searches")
  /// Privacy Policy
  internal static let privacyPolicy = L10n.tr("Localizable", "privacy_policy")
  /// Push notifications are turned off. Please enable push notifications to receive these alerts.
  internal static let pushNotificationMessage = L10n.tr("Localizable", "push_notification_message")
  /// Keep you up to date with the latest jobs.\nUpdate you about jobs you’ve applied to.\nLet you know when you’ve got a new message from an employer.
  internal static let pushprimerBullets = L10n.tr("Localizable", "pushprimer_bullets")
  /// Skip for now
  internal static let pushprimerButtonNotnow = L10n.tr("Localizable", "pushprimer_button_notnow")
  /// Enable push notifications
  internal static let pushprimerButtonRegister = L10n.tr("Localizable", "pushprimer_button_register")
  /// Indeed Jobs sends push notifications to:
  internal static let pushprimerSubtitle = L10n.tr("Localizable", "pushprimer_subtitle")
  /// Get Job Updates
  internal static let pushprimerTitle = L10n.tr("Localizable", "pushprimer_title")
  /// Saved Jobs
  internal static let quickActionsSavedJobs = L10n.tr("Localizable", "quick_actions_saved_jobs")
  /// Search
  internal static let quickActionsSearch = L10n.tr("Localizable", "quick_actions_search")
  /// Rate "Indeed Jobs"
  internal static let rate = L10n.tr("Localizable", "rate")
  /// We've detected some unusual activity. Please solve the reCAPTCHA below.
  internal static let rateLimitingErrorMessage = L10n.tr("Localizable", "rate_limiting_error_message")
  /// Unusual Activity
  internal static let rateLimitingErrorTitle = L10n.tr("Localizable", "rate_limiting_error_title")
  /// Recent searches
  internal static let recentSearches = L10n.tr("Localizable", "recent_searches")
  /// Employers are still hiring for many positions
  internal static let reengagementPushSubtitle = L10n.tr("Localizable", "reengagement_push_subtitle")
  /// Ready to work?
  internal static let reengagementPushTitle = L10n.tr("Localizable", "reengagement_push_title")
  /// By using Indeed, you agree and consent to our:
  internal static let regpromoConsentText = L10n.tr("Localizable", "regpromo_consent_text")
  /// By using Indeed, you agree and consent to our: <a {0}>Terms of Service</a> - <a {1}>Cookie Policy</a> - <a {2}>Privacy Policy</a>
  internal static let regpromoLegalText = L10n.tr("Localizable", "regpromo_legal_text")
  /// Reload
  internal static let reload = L10n.tr("Localizable", "Reload")
  /// Remind me later
  internal static let remindMeLater = L10n.tr("Localizable", "remind_me_later")
  /// Reset
  internal static let reset = L10n.tr("Localizable", "Reset")
  /// You'll have to restart if you apply again.
  internal static let restartApply = L10n.tr("Localizable", "restart_apply")
  /// We're sorry, but resume upload is not supported for your region and/or language.
  internal static let resumeUploadNotSupported = L10n.tr("Localizable", "resume_upload_not_supported")
  /// Retry
  internal static let retry = L10n.tr("Localizable", "Retry")
  /// If you've enjoyed using Indeed on your mobile device we'd love to hear from you.
  internal static let reviewMessage = L10n.tr("Localizable", "review_message")
  /// Tell us what you think
  internal static let reviewTitle = L10n.tr("Localizable", "review_title")
  /// Select all
  internal static let selectAll = L10n.tr("Localizable", "select_all")
  /// Select Country
  internal static let selectCountry = L10n.tr("Localizable", "select_country")
  /// Sorry, we cannot reach Indeed as the server is temporarily unavailable.
  internal static let serverConnectionErrorMessage = L10n.tr("Localizable", "server_connection_error_message")
  /// Connection Error
  internal static let serverConnectionErrorTitle = L10n.tr("Localizable", "server_connection_error_title")
  /// Settings
  internal static let settings = L10n.tr("Localizable", "Settings")
  /// Share
  internal static let share = L10n.tr("Localizable", "Share")
  /// Sign in
  internal static let signIn = L10n.tr("Localizable", "sign_in")
  /// Something went wrong and we were unable to sign you in.
  internal static let signinErrorMessage = L10n.tr("Localizable", "signin_error_message")
  /// Sign-in failed
  internal static let signinFailed = L10n.tr("Localizable", "signin_failed")
  /// Skip
  internal static let skipLogin = L10n.tr("Localizable", "skip_login")
  /// Terms
  internal static let terms = L10n.tr("Localizable", "terms")
  /// Terms of Service
  internal static let termsOfService = L10n.tr("Localizable", "terms_of_service")
  /// By using Indeed you agree to our new <a {0}>Privacy Policy</a>, <a {1}>Cookie Policy</a>, and <a {2}>Terms</a>, which we encourage you to review.
  internal static let termsOfServiceMessage = L10n.tr("Localizable", "terms_of_service_message")
  /// Try Again
  internal static let tryAgain = L10n.tr("Localizable", "try_again")
  /// Turn on notifications
  internal static let turnOnNotification = L10n.tr("Localizable", "turn_on_notification")
  /// Unable to connect to Indeed.
  internal static let unableToConnectMessage = L10n.tr("Localizable", "unable_to_connect_message")
  /// Upload this file?
  internal static let uploadConfirmationTitle = L10n.tr("Localizable", "upload_confirmation_title")
  /// Please try again in a few moments
  internal static let uploadErrorSubtitle = L10n.tr("Localizable", "upload_error_subtitle")
  /// An error occurred uploading your resume
  internal static let uploadErrorTitle = L10n.tr("Localizable", "upload_error_title")
  /// Indeed only stores one resume at a time. If you upload or create a new one, your existing resume and any changes you've made will be deleted
  internal static let uploadFileExistSubtitle = L10n.tr("Localizable", "upload_file_exist_subtitle")
  /// Replace your existing resume?
  internal static let uploadFileExistTitle = L10n.tr("Localizable", "upload_file_exist_title")
  /// Your resume is uploading
  internal static let uploadProcessingSubtitle = L10n.tr("Localizable", "upload_processing_subtitle")
  /// Resume Processing
  internal static let uploadProcessingTitle = L10n.tr("Localizable", "upload_processing_title")
  /// Upload Resume
  internal static let uploadResume = L10n.tr("Localizable", "upload_resume")
  /// Having a resume with Indeed will let you quickly apply for jobs
  internal static let uploadSigninSubtitle = L10n.tr("Localizable", "upload_signin_subtitle")
  /// Sign in or create an account to upload your resume
  internal static let uploadSigninTitle = L10n.tr("Localizable", "upload_signin_title")
  /// Make your resume public so employers can reach out to you.
  internal static let uploadSuccessSubtitle = L10n.tr("Localizable", "upload_success_subtitle")
  /// Upload Successful
  internal static let uploadSuccessTitle = L10n.tr("Localizable", "upload_success_title")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
