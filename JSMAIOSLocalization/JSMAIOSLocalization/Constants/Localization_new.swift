// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// Suggestions from your Indeed résumé
  internal static let a11yApplyKeyboardIcon = L10n.tr("a11y_apply_keyboard_icon", #"Suggestions from your Indeed résumé"#)
  /// %@ Indeed suggestion
  internal static func a11yApplyKeyboardSuggestion(_ p1: Any) -> String {
    return L10n.tr("a11y_apply_keyboard_suggestion", #"%@ Indeed suggestion"#, String(describing: p1))
  }
  /// secure and validated
  internal static let a11yHttpsEnabled = L10n.tr("a11y_https_enabled", #"secure and validated"#)
  /// page is loading
  internal static let a11yLoadingAnnouncement = L10n.tr("a11y_loading_announcement", #"page is loading"#)
  /// Menu
  internal static let a11yNavbarMenu = L10n.tr("a11y_navbar_menu", #"Menu"#)
  /// Messages
  internal static let a11yNavbarMessages = L10n.tr("a11y_navbar_messages", #"Messages"#)
  /// More actions
  internal static let a11yNavbarMore = L10n.tr("a11y_navbar_more", #"More actions"#)
  /// Notifications
  internal static let a11yNavbarNotifications = L10n.tr("a11y_navbar_notifications", #"Notifications"#)
  /// Saved Jobs
  internal static let a11yNavbarSavedjobs = L10n.tr("a11y_navbar_savedjobs", #"Saved Jobs"#)
  /// Open in Safari
  internal static let a11yOpenSafari = L10n.tr("a11y_open_safari", #"Open in Safari"#)
  /// Address
  internal static let a11yWebAddress = L10n.tr("a11y_web_address", #"Address"#)
  /// Account Settings
  internal static let accountSettings = L10n.tr("account_settings", #"Account Settings"#)
  /// Contact us
  internal static let appReviewContactUs = L10n.tr("app_review_contact_us", #"Contact us"#)
  /// Enjoying the Indeed app?
  internal static let appReviewEnjoyingIndeed = L10n.tr("app_review_enjoying_indeed", #"Enjoying the Indeed app?"#)
  /// Glad we're helping
  internal static let appReviewGladToHelp = L10n.tr("app_review_glad_to_help", #"Glad we're helping"#)
  /// That's good to know
  internal static let appReviewGoodToKnow = L10n.tr("app_review_good_to_know", #"That's good to know"#)
  /// Your feedback is valuable to us
  internal static let appReviewHearYourFeedback = L10n.tr("app_review_hear_your_feedback", #"Your feedback is valuable to us"#)
  /// Have thoughts to share?
  internal static let appReviewHelpUsImprove = L10n.tr("app_review_help_us_improve", #"Have thoughts to share?"#)
  /// Not really
  internal static let appReviewNotReally = L10n.tr("app_review_not_really", #"Not really"#)
  /// Yes
  internal static let appReviewYes = L10n.tr("app_review_yes", #"Yes"#)
  /// Apply
  internal static let apply = L10n.tr("Apply", #"Apply"#)
  /// Back
  internal static let back = L10n.tr("Back", #"Back"#)
  /// Cancel
  internal static let buttonCancel = L10n.tr("button_cancel", #"Cancel"#)
  /// Create an Account
  internal static let buttonCreateAccount = L10n.tr("button_create_account", #"Create an Account"#)
  /// Maybe later
  internal static let buttonLater = L10n.tr("button_later", #"Maybe later"#)
  /// Make my resume public
  internal static let buttonMakePublic = L10n.tr("button_make_public", #"Make my resume public"#)
  /// Replace my existing resume
  internal static let buttonReplaceResume = L10n.tr("button_replace_resume", #"Replace my existing resume"#)
  /// Sign In
  internal static let buttonSignin = L10n.tr("button_signin", #"Sign In"#)
  /// Try Again
  internal static let buttonTryAgain = L10n.tr("button_try_again", #"Try Again"#)
  /// Upload
  internal static let buttonUpload = L10n.tr("button_upload", #"Upload"#)
  /// Cancel
  internal static let cancel = L10n.tr("Cancel", #"Cancel"#)
  /// Do Not Sell My Personal Information
  internal static let ccpaMessage = L10n.tr("ccpa_message", #"Do Not Sell My Personal Information"#)
  /// Change Country
  internal static let changeCountry = L10n.tr("change_country", #"Change Country"#)
  /// *All references can be found here
  internal static let citationLinkText = L10n.tr("citation_link_text", #"*All references can be found here"#)
  /// Close
  internal static let close = L10n.tr("Close", #"Close"#)
  /// Company Reviews
  internal static let companyReviews = L10n.tr("company_reviews", #"Company Reviews"#)
  /// Confirm
  internal static let confirm = L10n.tr("Confirm", #"Confirm"#)
  /// Sorry, we cannot reach the internet. Please check your connection.
  internal static let connectionErrorMessage = L10n.tr("connection_error_message", #"Sorry, we cannot reach the internet. Please check your connection."#)
  /// Connection Error
  internal static let connectionErrorTitle = L10n.tr("connection_error_title", #"Connection Error"#)
  /// Contact us
  internal static let contactUs = L10n.tr("contact_us", #"Contact us"#)
  /// Continue as guest
  internal static let continueAsGuest = L10n.tr("continue_as_guest", #"Continue as guest"#)
  /// Continue without an account
  internal static let continueWithoutAnAccount = L10n.tr("continue_without_an_account", #"Continue without an account"#)
  /// Would you like to provide details?
  internal static let convReportDetailsAsk = L10n.tr("conv_report_details_ask", #"Would you like to provide details?"#)
  /// Report details
  internal static let convReportDetailsTitle = L10n.tr("conv_report_details_title", #"Report details"#)
  /// Enter a message
  internal static let convReportEnterPlaceholder = L10n.tr("conv_report_enter_placeholder", #"Enter a message"#)
  /// Maximum %1$@ characters, %2$@ characters remaining.
  internal static func convReportMaxChars(_ p1: Any, _ p2: Any) -> String {
    return L10n.tr("conv_report_max_chars", #"Maximum %1$@ characters, %2$@ characters remaining."#, String(describing: p1), String(describing: p2))
  }
  /// Why are you reporting this message?
  internal static let convReportQuestion = L10n.tr("conv_report_question", #"Why are you reporting this message?"#)
  /// Submit feedback
  internal static let convReportSubmit = L10n.tr("conv_report_submit", #"Submit feedback"#)
  /// Report
  internal static let convReportTitle = L10n.tr("conv_report_title", #"Report"#)
  /// Advertisement
  internal static let convReportTypeAdvertisement = L10n.tr("conv_report_type_advertisement", #"Advertisement"#)
  /// Tried to sell me something
  internal static let convReportTypeAdvertisementDescription = L10n.tr("conv_report_type_advertisement_description", #"Tried to sell me something"#)
  /// Fraud
  internal static let convReportTypeFake = L10n.tr("conv_report_type_fake", #"Fraud"#)
  /// Might be fake or someone posting as this company
  internal static let convReportTypeFakeDescription = L10n.tr("conv_report_type_fake_description", #"Might be fake or someone posting as this company"#)
  /// Misleading
  internal static let convReportTypeInaccurate = L10n.tr("conv_report_type_inaccurate", #"Misleading"#)
  /// Didn't match the job description requirements
  internal static let convReportTypeInaccurateDescription = L10n.tr("conv_report_type_inaccurate_description", #"Didn't match the job description requirements"#)
  /// Discriminatory or offensive
  internal static let convReportTypeOffensive = L10n.tr("conv_report_type_offensive", #"Discriminatory or offensive"#)
  /// Used biased or inappropriate language
  internal static let convReportTypeOffensiveDescription = L10n.tr("conv_report_type_offensive_description", #"Used biased or inappropriate language"#)
  /// Other
  internal static let convReportTypeOther = L10n.tr("conv_report_type_other", #"Other"#)
  /// It's something else
  internal static let convReportTypeOtherDescription = L10n.tr("conv_report_type_other_description", #"It's something else"#)
  /// Not a fit or did not apply
  internal static let convReportTypeUninterested = L10n.tr("conv_report_type_uninterested", #"Not a fit or did not apply"#)
  /// Won't pursue this opportunity
  internal static let convReportTypeUninterestedDescription = L10n.tr("conv_report_type_uninterested_description", #"Won't pursue this opportunity"#)
  /// You applied to this position on %@.
  internal static func conversationAppliedTime(_ p1: Any) -> String {
    return L10n.tr("conversation_applied_time", #"You applied to this position on %@."#, String(describing: p1))
  }
  /// Archive
  internal static let conversationArchive = L10n.tr("conversation_archive", #"Archive"#)
  /// Archive Conversation
  internal static let conversationArchiveLong = L10n.tr("conversation_archive_long", #"Archive Conversation"#)
  /// Archived
  internal static let conversationArchivedTitle = L10n.tr("conversation_archived_title", #"Archived"#)
  /// Done
  internal static let conversationListDone = L10n.tr("conversation_list_done", #"Done"#)
  /// Edit
  internal static let conversationListEdit = L10n.tr("conversation_list_edit", #"Edit"#)
  /// You have no archived messages
  internal static let conversationListEmptyArchive = L10n.tr("conversation_list_empty_archive", #"You have no archived messages"#)
  /// We weren’t able to load that information.
  internal static let conversationListErrorSubtitle = L10n.tr("conversation_list_error_subtitle", #"We weren’t able to load that information."#)
  /// Well, that was unexpected
  internal static let conversationListErrorTitle = L10n.tr("conversation_list_error_title", #"Well, that was unexpected"#)
  /// Find jobs
  internal static let conversationListFindJobs = L10n.tr("conversation_list_find_jobs", #"Find jobs"#)
  /// Reminder
  internal static let conversationListReminder = L10n.tr("conversation_list_reminder", #"Reminder"#)
  /// Refresh now
  internal static let conversationListRetry = L10n.tr("conversation_list_retry", #"Refresh now"#)
  /// Sign in to your Indeed account to view messages
  internal static let conversationListSignIn = L10n.tr("conversation_list_sign_in", #"Sign in to your Indeed account to view messages"#)
  /// Inbox
  internal static let conversationListTitle = L10n.tr("conversation_list_title", #"Inbox"#)
  /// Upload your resume
  internal static let conversationListUploadResume = L10n.tr("conversation_list_upload_resume", #"Upload your resume"#)
  /// When an employer contacts you, you will see messages here.
  internal static let conversationListWelcomeSubtitle = L10n.tr("conversation_list_welcome_subtitle", #"When an employer contacts you, you will see messages here."#)
  /// Welcome to Messages
  internal static let conversationListWelcomeTitle = L10n.tr("conversation_list_welcome_title", #"Welcome to Messages"#)
  /// More actions
  internal static let conversationMoreActions = L10n.tr("conversation_more_actions", #"More actions"#)
  /// %1$@ connected with you based on your <a %2$@>resume on Indeed</a>.
  internal static func conversationResumeContact(_ p1: Any, _ p2: Any) -> String {
    return L10n.tr("conversation_resume_contact", #"%1$@ connected with you based on your <a %2$@>resume on Indeed</a>."#, String(describing: p1), String(describing: p2))
  }
  /// Your message wasn't delivered.
  internal static let conversationSendFailed = L10n.tr("conversation_send_failed", #"Your message wasn't delivered."#)
  /// Unarchive Conversation
  internal static let conversationUnarchiveLong = L10n.tr("conversation_unarchive_long", #"Unarchive Conversation"#)
  /// Unknown
  internal static let conversationUnknownParticipant = L10n.tr("conversation_unknown_participant", #"Unknown"#)
  /// An error occurred while trying to update conversation.
  internal static let conversationUpdateError = L10n.tr("conversation_update_error", #"An error occurred while trying to update conversation."#)
  /// View Job
  internal static let conversationViewJob = L10n.tr("conversation_view_job", #"View Job"#)
  /// Write your response...
  internal static let conversationViewSendPlaceholder = L10n.tr("conversation_view_send_placeholder", #"Write your response..."#)
  /// You
  internal static let conversationYou = L10n.tr("conversation_you", #"You"#)
  /// Cookie Policy
  internal static let cookiePolicy = L10n.tr("cookie_policy", #"Cookie Policy"#)
  /// Create an account
  internal static let createAnAccount = L10n.tr("create_an_account", #"Create an account"#)
  /// Create Your Resume
  internal static let createYourResume = L10n.tr("create_your_resume", #"Create Your Resume"#)
  /// Delete Selected
  internal static let deleteSelected = L10n.tr("delete_selected", #"Delete Selected"#)
  /// Discard your application?
  internal static let discardApply = L10n.tr("discard_apply", #"Discard your application?"#)
  /// Dismiss
  internal static let dismissMessage = L10n.tr("dismiss_message", #"Dismiss"#)
  /// Edit
  internal static let edit = L10n.tr("edit", #"Edit"#)
  /// To access this, please visit the main Indeed site.
  internal static let employersBlockMessage = L10n.tr("employers_block_message", #"To access this, please visit the main Indeed site."#)
  /// It's available for anyone to find\nEmployers and other registered users can contact you through Indeed\nYour phone number and address stay hidden\nYou can make your resume private again at any time\nView our privacy policy
  internal static let explanationTextResumeReachable = L10n.tr("explanation_text_resume_reachable", #"It's available for anyone to find\nEmployers and other registered users can contact you through Indeed\nYour phone number and address stay hidden\nYou can make your resume private again at any time\nView our privacy policy"#)
  /// When your resume is public:
  internal static let explanationTitleResumeReachable = L10n.tr("explanation_title_resume_reachable", #"When your resume is public:"#)
  /// Find Jobs
  internal static let findJobs = L10n.tr("find_jobs", #"Find Jobs"#)
  /// Find Salaries
  internal static let findSalaries = L10n.tr("find_salaries", #"Find Salaries"#)
  /// Update
  internal static let forceUpgradeButton = L10n.tr("force_upgrade_button", #"Update"#)
  /// Indeed Jobs is out of date. Please visit the App Store to update to the latest version.
  internal static let forceUpgradeMessage = L10n.tr("force_upgrade_message", #"Indeed Jobs is out of date. Please visit the App Store to update to the latest version."#)
  /// Update Required
  internal static let forcedUpgradeTitle = L10n.tr("forced_upgrade_title", #"Update Required"#)
  /// Forward
  internal static let forward = L10n.tr("Forward", #"Forward"#)
  /// Account
  internal static let gnavMenuAccount = L10n.tr("gnav_menu_account", #"Account"#)
  /// Change Country
  internal static let gnavMenuChangecountry = L10n.tr("gnav_menu_changecountry", #"Change Country"#)
  /// Company Reviews
  internal static let gnavMenuCompanyreviews = L10n.tr("gnav_menu_companyreviews", #"Company Reviews"#)
  /// Create account
  internal static let gnavMenuCreateaccount = L10n.tr("gnav_menu_createaccount", #"Create account"#)
  /// DEBUG View
  internal static let gnavMenuDebug = L10n.tr("gnav_menu_debug", #"DEBUG View"#)
  /// Find Jobs
  internal static let gnavMenuFindjobs = L10n.tr("gnav_menu_findjobs", #"Find Jobs"#)
  /// Find Salaries
  internal static let gnavMenuFindsalaries = L10n.tr("gnav_menu_findsalaries", #"Find Salaries"#)
  /// Help Center
  internal static let gnavMenuHelpcenter = L10n.tr("gnav_menu_helpcenter", #"Help Center"#)
  /// My Contributions
  internal static let gnavMenuMycontributions = L10n.tr("gnav_menu_mycontributions", #"My Contributions"#)
  /// My Jobs
  internal static let gnavMenuMyjobs = L10n.tr("gnav_menu_myjobs", #"My Jobs"#)
  /// My Subscriptions
  internal static let gnavMenuMysubscriptions = L10n.tr("gnav_menu_mysubscriptions", #"My Subscriptions"#)
  /// Resume
  internal static let gnavMenuResume = L10n.tr("gnav_menu_resume", #"Resume"#)
  /// Sign In
  internal static let gnavSigninLink = L10n.tr("gnav_signin_link", #"Sign In"#)
  /// Have an account?
  internal static let gnavSigninPrompt = L10n.tr("gnav_signin_prompt", #"Have an account?"#)
  /// Have an account?
  internal static let haveAnAccount = L10n.tr("have_an_account", #"Have an account?"#)
  /// Help Center
  internal static let helpCenter = L10n.tr("help_center", #"Help Center"#)
  /// 30% of the global workforce is made up of active job seekers.
  internal static func hintJobSeekers(_ p1: Int) -> String {
    return L10n.tr("hint_job_seekers", #"30% of the global workforce is made up of active job seekers."#, p1)
  }
  /// Home
  internal static let home = L10n.tr("Home", #"Home"#)
  /// Inbox
  internal static let inbox = L10n.tr("inbox", #"Inbox"#)
  /// The country you selected is not supported at this time.
  internal static let invalidCountrySelection = L10n.tr("invalid_country_selection", #"The country you selected is not supported at this time."#)
  /// Get Email Updates
  internal static let jobAlertEmailUpdate = L10n.tr("job_alert_email_update", #"Get Email Updates"#)
  /// Join now
  internal static let joinNow = L10n.tr("join_now", #"Join now"#)
  /// Enable location services by going to Settings > Privacy > Location Services
  internal static let locationServicesMessage = L10n.tr("location_services_message", #"Enable location services by going to Settings > Privacy > Location Services"#)
  /// Location Services Off
  internal static let locationServicesOff = L10n.tr("location_services_off", #"Location Services Off"#)
  /// Location service is restricted on this device.
  internal static let locationServicesRestrictedMessage = L10n.tr("location_services_restricted_message", #"Location service is restricted on this device."#)
  /// An error occurred contacting the server. Please check your internet connection and try again.
  internal static let makeResumePublicErrorTitle = L10n.tr("make_resume_public_error_title", #"An error occurred contacting the server. Please check your internet connection and try again."#)
  /// Messages
  internal static let messages = L10n.tr("messages", #"Messages"#)
  /// Employer
  internal static let messagingGenericEmployer = L10n.tr("messaging_generic_employer", #"Employer"#)
  /// Unknown
  internal static let messagingUnknownName = L10n.tr("messaging_unknown_name", #"Unknown"#)
  /// My Jobs
  internal static let myJobs = L10n.tr("my_jobs", #"My Jobs"#)
  /// My Resume
  internal static let myResume = L10n.tr("my_resume", #"My Resume"#)
  /// My Subscriptions
  internal static let mySubscriptions = L10n.tr("my_subscriptions", #"My Subscriptions"#)
  /// The world's #1 job search site
  internal static let no1JobsearchSite = L10n.tr("no1_jobsearch_site", #"The world's #1 job search site"#)
  /// No thanks
  internal static let noThanks = L10n.tr("no_thanks", #"No thanks"#)
  /// Notifications
  internal static let notifications = L10n.tr("notifications", #"Notifications"#)
  /// OK
  internal static let ok = L10n.tr("OK", #"OK"#)
  /// Get started
  internal static let onboardingGetstarted = L10n.tr("onboarding_getstarted", #"Get started"#)
  /// Search millions of jobs in one place
  internal static let onboardingPage1 = L10n.tr("onboarding_page1", #"Search millions of jobs in one place"#)
  /// Access company reviews and salary information
  internal static let onboardingPage2 = L10n.tr("onboarding_page2", #"Access company reviews and salary information"#)
  /// Apply to jobs from your phone with your saved resume
  internal static let onboardingPage3 = L10n.tr("onboarding_page3", #"Apply to jobs from your phone with your saved resume"#)
  /// Take your application further by messaging employers
  internal static let onboardingPage4 = L10n.tr("onboarding_page4", #"Take your application further by messaging employers"#)
  /// Skip
  internal static let onboardingSkip = L10n.tr("onboarding_skip", #"Skip"#)
  /// optional
  internal static let `optional` = L10n.tr("optional", #"optional"#)
  /// Find Jobs near Current Location
  internal static let placeholderSearchField = L10n.tr("placeholder_searchField", #"Find Jobs near Current Location"#)
  /// Popular Searches
  internal static let popularSearches = L10n.tr("popular_searches", #"Popular Searches"#)
  /// Privacy Policy
  internal static let privacyPolicy = L10n.tr("privacy_policy", #"Privacy Policy"#)
  /// Push notifications are turned off. Please enable push notifications to receive these alerts.
  internal static let pushNotificationMessage = L10n.tr("push_notification_message", #"Push notifications are turned off. Please enable push notifications to receive these alerts."#)
  /// Keep you up to date with the latest jobs.\nUpdate you about jobs you’ve applied to.\nLet you know when you’ve got a new message from an employer.
  internal static let pushprimerBullets = L10n.tr("pushprimer_bullets", #"Keep you up to date with the latest jobs.\nUpdate you about jobs you’ve applied to.\nLet you know when you’ve got a new message from an employer."#)
  /// Skip for now
  internal static let pushprimerButtonNotnow = L10n.tr("pushprimer_button_notnow", #"Skip for now"#)
  /// Enable push notifications
  internal static let pushprimerButtonRegister = L10n.tr("pushprimer_button_register", #"Enable push notifications"#)
  /// Indeed Jobs sends push notifications to:
  internal static let pushprimerSubtitle = L10n.tr("pushprimer_subtitle", #"Indeed Jobs sends push notifications to:"#)
  /// Get Job Updates
  internal static let pushprimerTitle = L10n.tr("pushprimer_title", #"Get Job Updates"#)
  /// Saved Jobs
  internal static let quickActionsSavedJobs = L10n.tr("quick_actions_saved_jobs", #"Saved Jobs"#)
  /// Search
  internal static let quickActionsSearch = L10n.tr("quick_actions_search", #"Search"#)
  /// Rate "Indeed Jobs"
  internal static let rate = L10n.tr("rate", #"Rate "Indeed Jobs""#)
  /// We've detected some unusual activity. Please solve the reCAPTCHA below.
  internal static let rateLimitingErrorMessage = L10n.tr("rate_limiting_error_message", #"We've detected some unusual activity. Please solve the reCAPTCHA below."#)
  /// Unusual Activity
  internal static let rateLimitingErrorTitle = L10n.tr("rate_limiting_error_title", #"Unusual Activity"#)
  /// Recent searches
  internal static let recentSearches = L10n.tr("recent_searches", #"Recent searches"#)
  /// Employers are still hiring for many positions
  internal static let reengagementPushSubtitle = L10n.tr("reengagement_push_subtitle", #"Employers are still hiring for many positions"#)
  /// Ready to work?
  internal static let reengagementPushTitle = L10n.tr("reengagement_push_title", #"Ready to work?"#)
  /// By using Indeed, you agree and consent to our:
  internal static let regpromoConsentText = L10n.tr("regpromo_consent_text", #"By using Indeed, you agree and consent to our:"#)
  /// By using Indeed, you agree and consent to our: <a {0}>Terms of Service</a> - <a {1}>Cookie Policy</a> - <a {2}>Privacy Policy</a>
  internal static let regpromoLegalText = L10n.tr("regpromo_legal_text", #"By using Indeed, you agree and consent to our: <a {0}>Terms of Service</a> - <a {1}>Cookie Policy</a> - <a {2}>Privacy Policy</a>"#)
  /// Reload
  internal static let reload = L10n.tr("Reload", #"Reload"#)
  /// Remind me later
  internal static let remindMeLater = L10n.tr("remind_me_later", #"Remind me later"#)
  /// Reset
  internal static let reset = L10n.tr("Reset", #"Reset"#)
  /// You'll have to restart if you apply again.
  internal static let restartApply = L10n.tr("restart_apply", #"You'll have to restart if you apply again."#)
  /// We're sorry, but resume upload is not supported for your region and/or language.
  internal static let resumeUploadNotSupported = L10n.tr("resume_upload_not_supported", #"We're sorry, but resume upload is not supported for your region and/or language."#)
  /// Retry
  internal static let retry = L10n.tr("Retry", #"Retry"#)
  /// If you've enjoyed using Indeed on your mobile device we'd love to hear from you.
  internal static let reviewMessage = L10n.tr("review_message", #"If you've enjoyed using Indeed on your mobile device we'd love to hear from you."#)
  /// Tell us what you think
  internal static let reviewTitle = L10n.tr("review_title", #"Tell us what you think"#)
  /// Select all
  internal static let selectAll = L10n.tr("select_all", #"Select all"#)
  /// Select Country
  internal static let selectCountry = L10n.tr("select_country", #"Select Country"#)
  /// Sorry, we cannot reach Indeed as the server is temporarily unavailable.
  internal static let serverConnectionErrorMessage = L10n.tr("server_connection_error_message", #"Sorry, we cannot reach Indeed as the server is temporarily unavailable."#)
  /// Connection Error
  internal static let serverConnectionErrorTitle = L10n.tr("server_connection_error_title", #"Connection Error"#)
  /// Settings
  internal static let settings = L10n.tr("Settings", #"Settings"#)
  /// Share
  internal static let share = L10n.tr("Share", #"Share"#)
  /// Sign in
  internal static let signIn = L10n.tr("sign_in", #"Sign in"#)
  /// Something went wrong and we were unable to sign you in.
  internal static let signinErrorMessage = L10n.tr("signin_error_message", #"Something went wrong and we were unable to sign you in."#)
  /// Sign-in failed
  internal static let signinFailed = L10n.tr("signin_failed", #"Sign-in failed"#)
  /// Skip
  internal static let skipLogin = L10n.tr("skip_login", #"Skip"#)
  /// Terms
  internal static let terms = L10n.tr("terms", #"Terms"#)
  /// Terms of Service
  internal static let termsOfService = L10n.tr("terms_of_service", #"Terms of Service"#)
  /// By using Indeed you agree to our new <a {0}>Privacy Policy</a>, <a {1}>Cookie Policy</a>, and <a {2}>Terms</a>, which we encourage you to review.
  internal static let termsOfServiceMessage = L10n.tr("terms_of_service_message", #"By using Indeed you agree to our new <a {0}>Privacy Policy</a>, <a {1}>Cookie Policy</a>, and <a {2}>Terms</a>, which we encourage you to review."#)
  /// Try Again
  internal static let tryAgain = L10n.tr("try_again", #"Try Again"#)
  /// Turn on notifications
  internal static let turnOnNotification = L10n.tr("turn_on_notification", #"Turn on notifications"#)
  /// Unable to connect to Indeed.
  internal static let unableToConnectMessage = L10n.tr("unable_to_connect_message", #"Unable to connect to Indeed."#)
  /// Upload this file?
  internal static let uploadConfirmationTitle = L10n.tr("upload_confirmation_title", #"Upload this file?"#)
  /// Please try again in a few moments
  internal static let uploadErrorSubtitle = L10n.tr("upload_error_subtitle", #"Please try again in a few moments"#)
  /// An error occurred uploading your resume
  internal static let uploadErrorTitle = L10n.tr("upload_error_title", #"An error occurred uploading your resume"#)
  /// Indeed only stores one resume at a time. If you upload or create a new one, your existing resume and any changes you've made will be deleted
  internal static let uploadFileExistSubtitle = L10n.tr("upload_file_exist_subtitle", #"Indeed only stores one resume at a time. If you upload or create a new one, your existing resume and any changes you've made will be deleted"#)
  /// Replace your existing resume?
  internal static let uploadFileExistTitle = L10n.tr("upload_file_exist_title", #"Replace your existing resume?"#)
  /// Your resume is uploading
  internal static let uploadProcessingSubtitle = L10n.tr("upload_processing_subtitle", #"Your resume is uploading"#)
  /// Resume Processing
  internal static let uploadProcessingTitle = L10n.tr("upload_processing_title", #"Resume Processing"#)
  /// Upload Resume
  internal static let uploadResume = L10n.tr("upload_resume", #"Upload Resume"#)
  /// Having a resume with Indeed will let you quickly apply for jobs
  internal static let uploadSigninSubtitle = L10n.tr("upload_signin_subtitle", #"Having a resume with Indeed will let you quickly apply for jobs"#)
  /// Sign in or create an account to upload your resume
  internal static let uploadSigninTitle = L10n.tr("upload_signin_title", #"Sign in or create an account to upload your resume"#)
  /// Make your resume public so employers can reach out to you.
  internal static let uploadSuccessSubtitle = L10n.tr("upload_success_subtitle", #"Make your resume public so employers can reach out to you."#)
  /// Upload Successful
  internal static let uploadSuccessTitle = L10n.tr("upload_success_title", #"Upload Successful"#)
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ key: String, _ value: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: "Localizable")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
