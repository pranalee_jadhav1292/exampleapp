//
//  Localization.swift
//  JSMALocalization
//
//  Created by Pranalee Jadhav on 9/16/20.
//  Copyright © 2020 Pranalee Jadhav. All rights reserved.
//

import UIKit

// MARK: StoryboardLocalizable
public protocol StoryboardLocalizable {
    var localizationKey: String? { get set }
}

extension UILabel: StoryboardLocalizable {
    @IBInspectable public var localizationKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized()
        }
    }
}

extension UIButton: StoryboardLocalizable {
    @IBInspectable public var localizationKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized(), for: .normal)
        }
    }
}

extension UINavigationItem: StoryboardLocalizable {
    @IBInspectable public var localizationKey: String? {
        get { return nil }
        set(key) {
            title = key?.localized()
        }
    }
}

extension UIBarItem: StoryboardLocalizable {
    @IBInspectable public var localizationKey: String? {
        get { return nil }
        set(key) {
            title = key?.localized()
        }
    }
}
