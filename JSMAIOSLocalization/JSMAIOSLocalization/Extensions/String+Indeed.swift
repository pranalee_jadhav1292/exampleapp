//
//  String+Indeed.swift
//  JSMALocalization
//
//  Created by Pranalee Jadhav on 9/16/20.
//  Copyright © 2020 Pranalee Jadhav. All rights reserved.
//

import Foundation

extension String {

    // The function will be revisited after localization framework is incorporated inside the iphone project
    func localized(withComment comment: String = "") -> String {

        var result = Bundle.main.localizedString(forKey: self, value: nil, table: nil)

        if result == self {
            result = Bundle.main.localizedString(forKey: self, value: nil, table: "Default")
        }
        return result
    }
}
